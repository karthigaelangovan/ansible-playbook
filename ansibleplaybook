---

- name: run the playbook tasks on the localhost
  hosts: 127.0.0.1
  connection: local
  become: yes
  tasks:

    - name: Install apache packages
      apt: name=apache2 force_apt_get=yes state=present

    - name: enabled mod_rewrite
      apache2_module: name=rewrite state=present
      notify:
        - restart apache2

    - name: Download composer
      get_url:
        url: https://getcomposer.org/download/
        dest: /ansible

    - name: install composer
      apt: name=composer force_apt_get=yes state=present

    - name: install laravel
      command: composer create-project laravel/laravel laravel 5.7

    - name: Add php7.2 repo
      apt_repository:
       repo: 'ppa:ondrej/php'

    - name: Install list of packages
      apt:
        pkg:
          - php7.2
          - libapache2-mod-php7.2
          - php7.2-mbstring
          - php7.2-xmlrpc
          - php7.2-soap
          - php7.2-gd
          - php7.2-xml
          - php7.2-cli
          - php7.2-zip
          - php7.2-mysql
          - php7.2-ldap
          - php7.2-curl
        force_apt_get: yes

    - name: "Installing pip dependencies"
      pip:
        name: MySQL-python
        state: present

    - name: Installing mysql-server
      apt: pkg=mysql-server-5.7 force_apt_get=yes state=present

    - name: Start Mysql
      service: name=mysql state=started enabled=true

    - name: create database
      mysql_db: >
        name=mydb
        collation=utf8_swedish_ci
        encoding=utf8
        state=present

    - name: create db user
      mysql_user: name=mydb host="%" password=admin priv=*.*:ALL

    - name: Install a git package
      apt: name=git force_apt_get=yes  state=present

    - name: Install curl
      apt: name=curl force_apt_get=yes  state=present

    - name: Install openssh server
      apt: name=openssh-server force_apt_get=yes state=present

    - name: Install Python pexpert
      apt: name=python-pexpect force_apt_get=yes state=present

    - name: Allow everything and enable UFW
      ufw:
        state: enabled
        policy: allow

    - name: Allow all access to port frontend http
      ufw:
        rule: allow
        port: "80"

    - name: Allow all access to port frontend https
      ufw:
        rule: allow
        port: "443"

    - name: Allow all access to port backend
      ufw:
        rule: allow
        port: "8085"

    - name: Allow all access to port backend-kerberos
      ufw:
        rule: allow
        port: "8086"

    - name: Allow all access to port mysql
      ufw:
        rule: allow
        port: "3306"

    - name: download nodejs 10.x
      get_url:
        url: https://deb.nodesource.com/setup_10.x
        dest: /ansible

    - name: install nodejs
      apt: name=nodejs force_apt_get=yes  state=present

    - name: instll angular cli
      command: npm install -g @angular/cli

    - name: install libapache2-mod-auth-kerb
      apt: name=libapache2-mod-auth-kerb force_apt_get=yes state=present

    - name: install kerberos
      apt: name=krb5-user force_apt_get=yes  state=present

  handlers:
    - name: restart apache2
      service: name=apache2 state=restarted
